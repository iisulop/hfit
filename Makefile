CC = clang
CCFLAGS += -g -c -Wall -Wextra -pedantic -std=c99
CCFLAGS += -fPIC # needed for shared library

SOURCES := $(wildcard src/*.c)
ICNLUDES := $(wildcard src/*.h)
OBJECTS := $(SOURCES:src/%.c=obj/%.o)

all: directories obj/libhfit.a obj/libhfit.so

obj/libhfit.a: $(OBJECTS)
	ar rcs $@ $^

obj/libhfit.so: $(OBJECTS)
	$(CC) $(LDFLAGS) -shared $^ -o $@

$(OBJECTS): obj/%.o : src/%.c
	$(CC) $(CCFLAGS) -c $< -o $@

obj/:
	mkdir obj

lint:
	splint src/*.c src/*.h

.PHONY: directories clean

directories: obj/

clean:
	rm -rf obj/*.o obj/libhfit.a obj/libhfit.so
