#ifndef HFIT_NATIVE_TYPES_H
#define HFIT_NATIVE_TYPES_H

#include <stdint.h>

typedef uint8_t hfu8_t;
typedef uint16_t hfu16_t;
typedef uint32_t hfu32_t;
typedef uint64_t hfu64_t;
typedef int8_t hfi8_t;
typedef int16_t hfi16_t;
typedef int32_t hfi32_t;
typedef int64_t hfi64_t;

#endif // HFIT_NATIVE_TYPES_H
