#include "hfit_internal.h"
#include "hfit_common.h"

hfit_error_t
hfit_parse_definition_field (hfu8_t* definition_message,
                             hfit_field_definition_t* field_definition)
{
  hfit_error_t err = HFIT_OK;

  hfit_assert (field_definition != 0);
  hfit_memset ((hfu8_t*)field_definition, 0, sizeof (hfit_field_definition_t));

  field_definition->field_definition_number = definition_message[0];
  field_definition->size = definition_message[1];
  field_definition->base_type = definition_message[2];

  return err;
}
