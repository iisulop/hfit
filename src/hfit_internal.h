#ifndef HFIT_INTERNAL_H
#define HFIT_INTERNAL_H

#include <assert.h>

#include "hfit_general_types.h"

#define hfit_assert(condition) assert(condition)

// Error definitions
typedef enum {
  HFIT_OK,

  // Initialization errors
  HFIT_ERROR_NULL_HFIT,
  HFIT_ERROR_LEN_UNDER_MINIMUM,

  // Header checing errors
  HFIT_ERROR_HEADER_TOO_SMALL,
  HFIT_ERROR_UNSUPPORTED_PROTOCOL,
  HFIT_ERROR_UNSUPPORTED_PROFILE_VERSION,
  HFIT_ERROR_DATA_TYPE_NOT_DOT_FIT,

  // Definition message parsing errors
  HFIT_ERROR_UNSUPPORTED_ARCHITECTURE,
  // Data message parsing errors
  HFIT_ERROR_NULL_DATA_MESSAGE,

  // Buffer length errors
  HFIT_ERROR_BUFFER_OVERRUN,

  // CRC checking error
  HFIT_ERROR_CRC_FAIL,
} hfit_error_t;

// Definition message
typedef struct {
    hfu8_t local_message_type;
    hfu8_t architecture;
    hfu16_t global_message_number;
    hfu8_t num_fields;
    hfu8_t* fields; // Points to the beginning of definition fields
    hfu8_t num_developer_fields;
    hfu8_t* developer_fields; // Points to the beginning of developer fields
} hfit_definition_message_t;

typedef struct {
    // Data buffer info
    hfu8_t field_definition_number;
    hfu8_t size;
    fit_base_type_name_t base_type;
} hfit_field_definition_t;

typedef struct {
    // Data buffer info
    hfu8_t* buf;
    hfu64_t len;
    hfu8_t* end;
    hfu8_t* pos;

    // The currently applicable definition messages
    hfit_definition_message_t definition_messages[16];

    hfu64_t last_time;
} hfit_t;

typedef enum {
    HFIT_DEFINITION_MESSAGE,
    HFIT_DATA_MESSAGE
} message_type_t;

typedef struct {
    hfu8_t compressed_timestamp_header;
    hfu8_t local_message_type;
    message_type_t message_type;
    // time_offset if compressed_timestamp_header == 1
    // extended_definitions if compressed_timestamp_header == 0
    union {
        hfu8_t time_offset; // Compressed timestamp
        hfu8_t extended_definitions; // Normal definition message
    } type_dependent;

} hfit_record_header_t;

// Data message
typedef struct {
    hfu8_t nothing;
} hfit_data_message_t;

#endif // HFIT_INTERNAL_H
