#ifndef HFIT_HEADER_H
#define HFIT_HEADER_H

#include "hfit_internal.h"

hfit_error_t
hfit_check_header (hfit_t* hfit);

hfu32_t
hfit_get_data_size (hfit_t* hfit);

#endif // HFIT_HEADER_H
