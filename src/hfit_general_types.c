#include "hfit_general_types.h"

fit_base_type_t FIT_BASE_TYPES[] =
{
    { 0, 0, 0x00, HFIT_ENUM, 0xFF, 1 },
    { 1, 0, 0x01, HFIT_SINT8, 0x7F, 1 },
    { 2, 0, 0x02, HFIT_UINT8, 0xFF, 1 },
    { 3, 1, 0x83, HFIT_SINT16, 0x7FFF, 2 },
    { 4, 1, 0x84, HFIT_UINT16, 0xFFFF, 2 },
    { 5, 1, 0x85, HFIT_SINT32, 0x7FFFFFFF, 4 },
    { 6, 1, 0x86, HFIT_UINT32, 0xFFFFFFFF, 4 },
    { 7, 0, 0x07, HFIT_STRING, 0x00, 0 },
    { 8, 1, 0x88, HFIT_FLOAT32, 0xFFFFFFFF, 4 },
    { 9, 1, 0x89, HFIT_FLOAT64, 0xFFFFFFFFFFFFFFFF, 8 },
    { 10, 0, 0x0A, HFIT_UINT8Z, 0x00, 1 },
    { 11, 1, 0x8B, HFIT_UINT16Z, 0x0000, 2 },
    { 12, 1, 0x8C, HFIT_UINT32Z, 0x00000000, 4 },
    { 13, 0, 0x0D, HFIT_BYTE, 0xFF, 0 },
    { 14, 1, 0x8E, HFIT_SINT64, 0x0000000000000000, 8 },
    { 15, 1, 0x8F, HFIT_UINT64, 0x0000000000000000, 8 },
    { 16, 1, 0x90, HFIT_UINT64Z, 0x0000000000000000, 8 }
};
