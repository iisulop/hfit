#ifndef HFIT_H
#define HFIT_H

#include "hfit_internal.h"

hfit_error_t
hfit_init (hfit_t* hfit, hfu8_t* buf, hfu64_t len);

void
hfit_uninit (hfit_t* hfit);

hfit_error_t
hfit_next_data_message (hfit_t* hfit, hfit_data_message_t* data_message);

#endif // HFIT_H
