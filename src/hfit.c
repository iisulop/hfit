#include "hfit.h"
#include "hfit_header.h"
#include "hfit_internal.h"
#include "hfit_common.h"
#include "hfit_record_parser.h"

hfit_error_t
hfit_init (hfit_t* hfit, hfu8_t* buf, hfu64_t len)
{
  hfit_error_t err = HFIT_OK;

  if (hfit == 0)
    {
      return HFIT_ERROR_NULL_HFIT;
    }

  hfit_memset ((hfu8_t*)hfit, 0, sizeof (hfit_t));
  hfit->buf = buf;
  hfit->len = len;
  hfit->end = buf + len;

  err = hfit_check_header (hfit);
  if (err != HFIT_OK)
    {
      hfit_uninit (hfit);
    }

  return err;
}

void
hfit_uninit (hfit_t* hfit)
{
  if (hfit)
    {
      hfit_memset ((hfu8_t*)hfit, 0, sizeof (hfit_t));
    }
}

hfit_error_t
hfit_next_data_message (hfit_t* hfit, hfit_data_message_t* data_message)
{
  if (hfit == 0)
    {
      return HFIT_ERROR_NULL_HFIT;
    }
  else if (data_message == 0)
    {
      return HFIT_ERROR_NULL_DATA_MESSAGE;
    }

  hfit_get_next_data_message (hfit, data_message);
  return HFIT_OK;
}
