#ifndef HFIT_RECORD_PARSER_H
#define HFIT_RECORD_PARSER_H

#include "hfit_internal.h"

hfit_error_t
hfit_get_next_data_message (hfit_t* hfit, hfit_data_message_t* data_message);

// Definition messages could be saved by just saving pointer to the beginning
// of the message

#endif // HFIT_RECORD_PARSER_H

