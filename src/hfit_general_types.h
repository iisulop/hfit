#ifndef HFIT_GENERAL_TYPES
#define HFIT_GENERAL_TYPES

#include "hfit_native_types.h"

#define NUM_FIT_BASE_TYPES 17

typedef enum {
    HFIT_ENUM,
    HFIT_SINT8,
    HFIT_UINT8,
    HFIT_SINT16,
    HFIT_UINT16, HFIT_SINT32,
    HFIT_UINT32,
    HFIT_STRING,
    HFIT_FLOAT32,
    HFIT_FLOAT64,
    HFIT_UINT8Z,
    HFIT_UINT16Z,
    HFIT_UINT32Z,
    HFIT_BYTE,
    HFIT_SINT64,
    HFIT_UINT64,
    HFIT_UINT64Z,
} fit_base_type_name_t;

typedef struct {
    hfu8_t base_type;
    hfu8_t endian_ability;
    hfu8_t base_type_field;
    fit_base_type_name_t type_name;
    hfu64_t invalid_value;
    hfu8_t size;
} fit_base_type_t;

fit_base_type_t FIT_BASE_TYPES[NUM_FIT_BASE_TYPES];

#endif // FIT_GENERAL_TYPES
