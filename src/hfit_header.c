#include "hfit_common.h"
#include "hfit_header.h"
#include "hfit_internal.h"

static const uint8_t HFIT_MIN_HEADER_SIZE = 12;

static const uint8_t HEADER_SIZE_OFFSET = 0;
static const uint8_t HEADER_PROTOCOL_VERSION_OFFSET = 1;
static const uint8_t HEADER_PROFILE_VERSION_OFFSET = 2;
static const uint8_t HEADER_DATA_SIZE_OFFSET = 4;
static const uint8_t HEADER_DATA_TYPE_OFFSET = 8;

static const char* EXPECTED_TYPE = ".FIT";

static const uint8_t HFIT_MAX_SUPPORTED_PROTOCOL_VERSION = 0x20;
static const uint16_t HFIT_SUPPORTED_PROFILE_VERSION = (100*20) + 54;

static hfu8_t
get_header_size (hfit_t* hfit)
{
  hfit_assert (hfit != 0);
  return hfit->buf[HEADER_SIZE_OFFSET];
}

static hfu8_t
get_protocol_version (hfit_t* hfit)
{
  hfit_assert (hfit != 0);
  return hfit->buf[HEADER_PROTOCOL_VERSION_OFFSET];
}

static hfu16_t
get_profile_version (hfit_t* hfit)
{
  hfit_assert (hfit != 0);
  hfu16_t val = 0;
  val += (hfu32_t)hfit->buf[HEADER_PROFILE_VERSION_OFFSET];
  val += ((hfu32_t)hfit->buf[HEADER_PROFILE_VERSION_OFFSET + 1]) << 8;
  return val;
}

static hfu8_t*
get_data_type (hfit_t* hfit)
{
  hfit_assert (hfit != 0);
  return &(hfit->buf[HEADER_DATA_TYPE_OFFSET]);
}

hfit_error_t
hfit_check_header (hfit_t* hfit)
{
  hfit_assert (hfit != 0);
  if (get_header_size (hfit) < HFIT_MIN_HEADER_SIZE)
    {
      return HFIT_ERROR_HEADER_TOO_SMALL;
    }
  else if (get_protocol_version (hfit) > HFIT_MAX_SUPPORTED_PROTOCOL_VERSION)
    {
      return HFIT_ERROR_UNSUPPORTED_PROTOCOL;
    }
  /*
  else if (get_profile_version (hfit) != HFIT_SUPPORTED_PROFILE_VERSION)
    {
      return HFIT_ERROR_UNSUPPORTED_PROFILE_VERSION;
    }
    */
  else if (hfit_memcmp (get_data_type (hfit), (hfu8_t*)EXPECTED_TYPE, 4) != 0)
    {
      return HFIT_ERROR_DATA_TYPE_NOT_DOT_FIT;
    }

  hfit->pos += get_header_size (hfit);

  return HFIT_OK;
}

hfu32_t
hfit_get_data_size (hfit_t* hfit)
{
  hfit_assert (hfit != 0);
  hfu32_t val = 0;
  val = (hfu16_t)hfit->buf[HEADER_DATA_SIZE_OFFSET];
  val += ((hfu16_t)hfit->buf[HEADER_DATA_SIZE_OFFSET + 1]) << 8;
  return val;
}

