#include "hfit_record_parser.h"
#include "hfit_common.h"

static const hfu8_t HEADER_TYPE_MASK = 0x80;
static const hfu8_t MESSAGE_TYPE_MASK = 0x40;
static const hfu8_t LOCAL_MESSAGE_TYPE_MASK = 0x07;
static const hfu8_t LOCAL_MESSAGE_TYPE_COMPRESSED_MASK = 0x60;
static const hfu8_t LOCAL_MESSAGE_TYPE_COMPRESSED_SHIFT = 5;
static const hfu8_t COMPRESSED_TIME_OFFSET_MASK = 0x1F;
static const hfu8_t EXTENDED_DEFINITIONS_MASK = 0x10;

/**
Record header format
+---------------+-------------+----------------+----------+----------------+
|     Bit 8     |    Bit 7    |     Bit 6      |  Bit 5   |    Bits 4-1    |
+---------------+-------------+----------------+----------+----------------+
| Header Type   | Msg Type    | Def Msg only:  | Reserved | Local Msg Type |
| 0: Normal     | 0: Data Msg | 1: Dev data    |          |                |
| 1: Compressed | 1: Def Msg  | 0: No Dev data |          |                |
+---------------+-------------+----------------+----------+----------------+
*/
static hfit_error_t
parse_record_header (hfit_t* hfit, hfit_record_header_t* record_header)
{
  hfu8_t header = 0;
  hfit_error_t err = hfit_get8bits (hfit, &header);

  hfit_assert (record_header != 0);
  hfit_memset ((hfu8_t*)record_header, 0, sizeof (hfit_record_header_t));

  if (err != HFIT_OK)
    {
      return err;
    }

  if ((header & HEADER_TYPE_MASK) == 0x80) // Compressed timestamp header
    {
      // Message type is implicitly data
      record_header->compressed_timestamp_header = 1;
      record_header->message_type = HFIT_DATA_MESSAGE;
      record_header->local_message_type =
        (header & LOCAL_MESSAGE_TYPE_COMPRESSED_MASK) >>
          LOCAL_MESSAGE_TYPE_COMPRESSED_SHIFT;
      record_header->type_dependent.time_offset =
        (header & COMPRESSED_TIME_OFFSET_MASK);
    }
  else // Normal header
    {
      record_header->local_message_type = (header & LOCAL_MESSAGE_TYPE_MASK);
      record_header->message_type =
        (header & MESSAGE_TYPE_MASK) == 0 ?
          HFIT_DATA_MESSAGE : HFIT_DEFINITION_MESSAGE;
      if (record_header->message_type == HFIT_DEFINITION_MESSAGE)
        {
          record_header->type_dependent.extended_definitions =
            (header & EXTENDED_DEFINITIONS_MASK) == 0 ? 0 : 1;
        }
    }

  return HFIT_OK;
}

static hfit_error_t
process_definition_message (hfit_t* hfit, hfit_record_header_t* record_header)
{
  hfit_error_t err = HFIT_OK;
  hfit_definition_message_t definition_message;

  hfit_assert (record_header->local_message_type < 16);

  hfit_memset ((hfu8_t*)&definition_message, 0,
               sizeof (hfit_definition_message_t));

  definition_message.local_message_type = record_header->local_message_type;

  err = hfit_get8bits (hfit, &definition_message.architecture);
  if (err != HFIT_OK)
    {
      return err;
    }

  err = hfit_get16bits (hfit, definition_message.architecture,
                        &definition_message.global_message_number);
  if (err != HFIT_OK)
    {
      return err;
    }

  err = hfit_get8bits (hfit, &definition_message.num_fields);
  if (err != HFIT_OK)
    {
      return err;
    }
  // Points to # of Fields + 1, ie. first definition
  definition_message.fields = hfit->pos;
  // Move pos over the field definitions
  hfit_increment_pos (hfit, definition_message.num_fields * 3);

  // Parse the developer data fields if those are included
  if (record_header->type_dependent.extended_definitions == 1)
    {
      err = hfit_get8bits (hfit, &definition_message.num_developer_fields);
      if (err != HFIT_OK)
        {
          return err;
        }
      // Points to # of Developer Fields + 1, ie. first definition
      definition_message.developer_fields = hfit->pos;
      // Move pos over the developer field definitions
      hfit_increment_pos (hfit, definition_message.num_developer_fields * 3);
    }

  // Save the new definition. Existing one with same local message type
  // will be overridden as described in the FIT Protocol spec.
  hfit->definition_messages[record_header->local_message_type] =
    definition_message;

  return err;
}

/*
static hfit_error_t
process_compressed_timestamp_message (hfit_t* hfit,
                                      hfit_record_header_t* record_header,
                                      hfit_data_message_t* data_message)
{
}

static hfit_error_t
process_data_message (hfit_t* hfit, hfit_record_header_t* record_header,
                      hfit_data_message_t* data_message)
{
}
*/

hfit_error_t
hfit_get_next_data_message (hfit_t* hfit, hfit_data_message_t* data_message)
{
  hfit_error_t err = HFIT_OK;
  hfit_record_header_t record_header;

  hfit_assert (hfit != 0);
  hfit_assert (data_message != 0);

  hfit_memset ((hfu8_t*)data_message, 0, sizeof (hfit_data_message_t));

  // Parse records until a data message is found
  do {
      err = parse_record_header (hfit, &record_header);
      if (err != HFIT_OK)
        {
          return err;
        }
      if (record_header.message_type == HFIT_DEFINITION_MESSAGE)
        {
          process_definition_message (hfit, &record_header);
        }
    } while (record_header.message_type == HFIT_DEFINITION_MESSAGE);

  /*
  if (record_header.compressed_timestamp_header)
    {
      process_compressed_timestamp_message (hfit, &record_header, data_message);
    }
  else if (record_header.message_type == HFIT_DATA_MESSAGE)
    {
      process_data_message (hfit, &record_header, data_message);
    }
    */

  return err;
}
