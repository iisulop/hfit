#include "hfit_native_types.h"
#include "hfit_internal.h"

static hfu8_t
check_len (hfit_t* hfit, hfu8_t len)
{
  return hfit->pos + len < hfit->end;
}

hfit_error_t
hfit_get8bits (hfit_t* hfit, hfu8_t* val)
{
  hfit_assert (hfit != 0);
  hfit_assert (val != 0);

  if (check_len (hfit, 1))
    {
      return HFIT_ERROR_BUFFER_OVERRUN;
    }

  *val = (hfu8_t)*hfit->pos++;

  return HFIT_OK;
}

hfit_error_t
hfit_get16bits (hfit_t* hfit, hfu8_t architecture, hfu16_t* val)
{
  hfit_assert (hfit != 0);
  hfit_assert (val != 0);

  if (check_len (hfit, 2))
    {
      return HFIT_ERROR_BUFFER_OVERRUN;
    }

  if (architecture == 0)
    {
      *val = (hfu16_t)*hfit->pos++;
      *val += ((hfu16_t)*hfit->pos++) << 8;
    }
  else if (architecture == 1)
    {
      *val = ((hfu16_t)*hfit->pos++) << 8;
      *val += (hfu16_t)*hfit->pos++;
    }
  else
    {
      return HFIT_ERROR_UNSUPPORTED_ARCHITECTURE;
    }

  return HFIT_OK;
}

hfit_error_t
hfit_get32bits (hfit_t* hfit, hfu8_t architecture, hfu32_t* val)
{
  hfit_assert (hfit != 0);
  hfit_assert (val != 0);

  if (check_len (hfit, 4))
    {
      return HFIT_ERROR_BUFFER_OVERRUN;
    }

  if (architecture == 0)
    {
      *val = (hfu32_t)*hfit->pos++;
      *val += ((hfu32_t)*hfit->pos++) << 8;
      *val += ((hfu32_t)*hfit->pos++) << 16;
      *val += ((hfu32_t)*hfit->pos++) << 24;
    }
  else if (architecture == 1)
    {
      *val = ((hfu32_t)*hfit->pos++) << 24;
      *val += ((hfu32_t)*hfit->pos++) << 16;
      *val += ((hfu32_t)*hfit->pos++) << 8;
      *val += (hfu32_t)*hfit->pos++;
    }
  else
    {
      return HFIT_ERROR_UNSUPPORTED_ARCHITECTURE;
    }

  return HFIT_OK;
}

hfit_error_t
hfit_increment_pos (hfit_t* hfit, hfu32_t num)
{
  if (check_len (hfit, num))
    {
      return HFIT_ERROR_BUFFER_OVERRUN;
    }

  hfit->pos += num;

  return HFIT_OK;
}

void
hfit_memset (hfu8_t* buf, hfu8_t c, hfu32_t n)
{
  hfit_assert (buf != 0);

  hfu32_t i = 0;
  for (; i < n; ++i)
    {
      buf[i] = c;
    }
}

hfi8_t
hfit_memcmp (const hfu8_t* buf, const hfu8_t* other, hfu32_t len)
{
  hfit_assert (buf != 0);
  hfit_assert (other != 0);

  hfu32_t i = 0;
  for (; i < len; ++i)
    {
      if (buf[i] != other[i])
        {
          return (hfi8_t)buf[i] - (hfi8_t)other[i];
        }
    }
  return 0;
}

hfit_error_t
hfit_check_crc (const hfu8_t* buf)
{
  if (buf[0] != 0 || buf[1] != 0)
    {
      // implement actual checking
    }
  // CRC has been set to zero
  return 0;
}
