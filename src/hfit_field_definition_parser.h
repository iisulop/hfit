#ifndef FIELD_DEFINITION_PARSER_H
#define FIELD_DEFINITION_PARSER_H

#include "hfit_internal.h"

hfit_error_t
hfit_parse_definition_field (hfu8_t* definition_message,
                             hfit_field_definition_t* field_definition);

#endif // FIELD_DEFINITION_PARSER_H
