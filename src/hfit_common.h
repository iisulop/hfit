#ifndef HFIT_COMMON_H
#define HFIT_COMMON_H

#include "hfit_native_types.h"
#include "hfit_internal.h"

hfit_error_t
hfit_get8bits (hfit_t* hfit, hfu8_t* val);

#include "hfit_internal.h"

hfit_error_t
hfit_get8bits (hfit_t* hfit, hfu8_t* val);

hfit_error_t
hfit_get16bits (hfit_t* hfit, hfu8_t architecture, hfu16_t* val);

hfit_error_t
hfit_get32bits (hfit_t* hfit, hfu8_t architecture, hfu32_t* val);

hfit_error_t
hfit_increment_pos (hfit_t* hfit, hfu32_t num);

void
hfit_memset (hfu8_t* buf, hfu8_t c, hfu32_t n);

hfi8_t
hfit_memcmp (const hfu8_t* buf, const hfu8_t* other, hfu32_t len);

hfit_error_t
check_crc (const hfu8_t* buf);

#endif // HFIT_COMMON_H
